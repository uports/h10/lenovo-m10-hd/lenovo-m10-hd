![pipeline status](https://gitlab.com/uports/h10/lenovo-m10-hd/lenovo-m10-hd/badges/main/pipeline.svg)
# Lenovo Tab M10 (2nd Gen) WiFi TB-X306F
______________________

![](./refs/m10hd.png)

Lenovo Tab M10 HD (amar_row_wifi) specs
==========================================


| Basic                   | Spec Sheet                                                                                                                     |
| -----------------------:|:------------------------------------------------------------------------------------------------------------------------------ |
| CPU                     | Octa-core (4x2.3 GHz Cortex-A53 & 4x1.8 GHz Cortex-A53)                                                                                                                      |
| Chipset                 | Mediatek MT6762 Helio P22T (12 nm)                                                                                                            |
| GPU                     | PowerVR GE8320610                                                                                                                   |
| Memory                  | 4 GB RAM                                                                                                                     |
| Shipped Android Version | Android 10                                                                                                                           |
| Storage                 |64GB                                                                                                                  |
| Battery                 | Li-Po 5000 mAh, non-removable battery                                                                                           |
| Display                 | 800 x 1280 pixels, 16:10 ratio (~149 ppi density)                                                                            |
| Camera (Back)(Main)     | 8 MP, AF                                                                                |
| Camera (Front)          | 5 MP
| Kernel Version          | 4.9.190 "Roaring Lionus"

# What works so far?

### Progress
![99%](https://progress-bar.dev/99) Ubuntu 20.04 Focal

- [X] Recovery
- [X] Boot
- [X] Bluetooth
- [X] Camera Fotos and Video Recording
- [x] GPS
- [X] Audio works
- [X] Bluetooth Audio
- [X] Waydroid
- [X] MTP
- [X] ADB
- [X] SSH
- [X] Online charge
- [X] Offline Charge
- [X] Wifi
- [X] SDCard
- [ ] Wireless display it connectes and brodcast image but then freezes
- [X] Manual Brightness Works
- [X] Hardware video playback
- [X] Rotation
- [X] Proximity sensor
- [X] Virtualization
- [X] GPU
- [X] Lightsensor
- [X] Proximity sensor
- [X] Automatic brightness
- [X] Hotspot
- [X] Airplane Mode

## Install:

- Download The latest devel-flashable-focal image from latest CI builds
https://gitlab.com/uports/h10/lenovo-m10-hd/lenovo-m10-hd/-/pipelines

- Flash the Ubuntu boot.img:
``fastboot flash boot ./boot.img ``

- Download [vbmeta.img](https://github.com/rubencarneiro/amar_row_wifi/releases/download/1.0/vbmeta.img)
``fastboot --disable-verification flash vbmeta ./vbmeta.img ``
- Reboot to fastboot
``fastboot reboot fastboot``
- Format userdata
``fastboot format:ext4 userdata``
- Delete product logical partition
``fastboot delete-logical-partition product``
- Resise system partition
``fastboot resize-logical-partition system 3978565472``
- Flash Ubuntu Rootfs:
`` fastboot flash system ./ubuntu.img``


__________________________________

## Im not endorsed or founded by UBports Foundation and i do this for fun but does take time work and resources, so if you want to support and apreciate my work pleaso do Donate:
___________________________________
<center>
<a href="https://paypal.me/rubencarneiro?locale.x=pt_PT" title="PayPal" onclick="javascript:window.open('https://paypal.me/rubencarneiro?locale.x=pt_PT','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/Logo/pp-logo-150px.png" border="0" alt="PayPal Logo"></a>
